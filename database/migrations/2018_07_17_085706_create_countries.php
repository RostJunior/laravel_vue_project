<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('countries')) {
            Schema::table('countries', function (Blueprint $table) {

                $table->increments('id');
                $table->integer('region_id');
                $table->string('flag', 50)->nullable();
                $table->string('name', 255)->nullable();

                $table->foreign('region_id')->references('id')->on('country_groups');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
