<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournaments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tournaments')) {
            Schema::table('tournaments', function (Blueprint $table) {

                $table->increments('id');
                $table->integer('country_id');
                $table->integer('tournament_group_id');
                $table->string('name', 255)->nullable();

                $table->foreign('tournament_group_id')->references('id')->on('tournament_groups');
                $table->foreign('country_id')->references('id')->on('countries');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
