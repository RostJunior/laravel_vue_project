<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>BetHint - main page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>

<body>
@yield('content')
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>


<script src="{{asset('js/app.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
<style>
    .nopadding {
        padding: 0;
    }

    .market-table {
        margin-top: 10px;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    }

    .market-table__header {
        background: #4a525c;
        border: 1px solid #979797;
    }

    .market-table__header_odds {
        display: table-row-group;
    }

    .market-table__link {
        padding: 10px 8px;
        font-family: 'Roboto';
        color: #a8a8a8;
        border-right: 1px solid #979797;
        min-width: 57px;
        width: auto;
        text-align: center;
        cursor: pointer;
        -webkit-transition: 0.4s all;
        -o-transition: 0.4s all;
        transition: 0.4s all;
    }

    .market-table__link_active {
        background: #20ad8a;
        color: #fff;
    }

    .market-table__link:hover {
        background: #20ad8a;
        color: #fff;
    }

    .market-table__body {
        margin: 25px 0;
    }

    .market-table__body_odds {
        display: table-row-group;
    }

    .market-table__body_all-odds {
        height: 144px;
        overflow-y: auto;
    }

    .market-table__row {
        display: table-row;
    }

    .market-table__cell {
        display: table-cell;
        color: #a8a8a8;
        font-family: 'Roboto';
        font-size: 12px;
        font-weight: 500;
        border: 1px solid #979797;
        padding: 8px 10px;
        text-align: center;
        line-height: normal;
    }

    .market-table__cell_header {
        border: none;
        padding-top: 4px;
        padding-bottom: 4px;
    }

    .market-table__cell_big {
        padding: 4px 13px;
    }

    .market-table__odds {
        display: table;
        border-collapse: collapse;
        border: 1px solid #979797;
    }

    .market-table__wrap {
        height: 140px;
        overflow-y: auto;
        overflow-x: hidden;
        width: 24%;
    }

    .market-table__wrap::-webkit-scrollbar {
        width: 9px;
        background-color: #979797;
    }

    .market-table__wrap::-webkit-scrollbar-button {
        display: none;
    }

    .market-table__wrap::-webkit-scrollbar-thumb {
        background: #39404a;
        -webkit-border-radius: 20px;
        border-radius: 20px;
    }
</style>

</body>
</html>