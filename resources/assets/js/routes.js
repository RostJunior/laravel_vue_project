import Vue from 'vue'
import Router from 'vue-router'
import AboutUs from './components/AboutUs'
import Faq from './components/Faq'
import Contacts from './components/Contacts'
import Articles from './components/Articles'
import Article from './components/Article'
import Tournaments from './components/Tournaments'
import Home from './components/Home'
import ErrorPage from './components/ErrorPage'
import GamePage from './components/GamePage'
import store from './store'

Vue.use(Router)

export default new Router({

    routes: [
        {
            path: '',
            name: 'home',
            component: Home
        },
        {
            path: '/tournament/:id',
            props: true,
            name: 'tournament',
            component: Tournaments,
            beforeEnter(to, from, next) {
                store.dispatch('getTournament', to.params.id)

                if(store.getters.tournament) {
                    next()
                }else {
                    to.redirect('/')
                }

            }
        },
        {
            path: '/about-us',
            name: 'about_us',
            component: AboutUs
        },
        {
            path: '/faq',
            name: 'faq',
            component: Faq
        },
        {
            path: '/articles',
            name: 'articles',
            component: Articles
        },
        {
            path: '/article/one',
            name: 'article',
            component: Article
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: Contacts
        },
        {
            path: '/game/:id',
            name: 'game_info',
            component: GamePage
        },
        {
            path: '*',
            name: 'error',
            component: ErrorPage
        }
    ],
    mode: 'history'
})