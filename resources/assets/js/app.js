
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import App from './components/App'
import store from './store'
import router from './routes'


require('./bootstrap');

window.Vue = require('vue');
window.Vuetify = require('vuetify');


Vue.use(Vuetify)

Vue.component('appHeader', require('./components/Header'));


const app = new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    template: '<App/>',
    created () {
        this.$store.dispatch('getMenu')
        this.$store.dispatch('getTop')
    }

});
