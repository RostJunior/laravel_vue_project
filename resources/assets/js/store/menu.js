
export default {
    state: {
        menu: ''
    },
    mutations: {
        setMenu (state, payload) {
            state.menu = payload
        }
    },
    actions: {
        async getMenu ({commit}) {
            commit('clearError')
            commit('setLoading', true)
            try {
                const menu = await axios.get('/api/menu')
                    .then((response) => {
                        return response.data

                })
                commit('setMenu', menu.result)
                commit('setLoading', false)
            } catch (error) {
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        }
    },
    getters: {
        menu (state) {
            return state.menu
        }
    }
}