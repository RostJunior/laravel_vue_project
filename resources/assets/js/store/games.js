export default {
    state: {
        game: '',
        top_games: [],
        tournament: {tournament: 'Tournament', games: [], country: 'Country'}
    },
    mutations: {
        setTop (state, payload) {
            state.top_games = payload
        },
        setTournament (state, payload) {
            state.tournament = payload
        }

    },
    actions: {
        async getTop ({commit}) {
            commit('clearError')
            commit('setLoading', true)
            try {
                const top = await axios.get('/api/games/top')
                    .then((response) => {
                        return response.data

                    })
                commit('setTop', top.result)
                commit('setLoading', false)
            } catch (error) {
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        },
        async getTournament ({commit}, payload) {

            commit('clearError')
            commit('setLoading', true)
            const url = '/api/games/tournament/' +payload
            try {
                const top = await axios.get(url)
                    .then((response) => {
                        return response.data

                    })
                commit('setTournament', top.result)
                commit('setLoading', false)
            } catch (error) {
                commit('setError', error.message)
                commit('setLoading', false)
                throw error
            }
        }

    },
    getters: {
        game (state) {
            return state.game
        },
        top_games (state) {
            return state.top_games
        },
        tournament (state) {
            return state.tournament
        }
    }
}