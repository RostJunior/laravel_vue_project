<?php
/**
 * Created by PhpStorm.
 * User: RostT
 * Date: 17.07.2018
 * Time: 12:15
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Games;
use App\Models\Tournaments;

class ApiController extends Controller
{


    /**
     * Get left menu
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMenu() {
        $menu = Tournaments::getMenu();

        if($menu) {
            return response()->json(['status' => '1','result' => $menu]);
        }

        return response()->json(['status' => '0','result' => $menu]);
    }

    /**
     * Get Top games
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTopGames() {
        $top_games = Games::getTopGames();
        if($top_games->isNotEmpty()) {
            return response()->json(['status' => '1','result' => $top_games]);
        } else {
            return response()->json(['status' => '0','result' => false]);
        }
    }

    public function getTournamentGames($id) {
        $games = Games::getTournamentGames($id);
        if($games->isNotEmpty()) {
            return response()->json(['status' => '1','result' => [
                'tournament' => $games[0]->tournament,
                'country' => $games[0]->country,
                'games' => $games
            ]]);
        } else {
            return response()->json(['status' => '0','result' => false]);
        }
    }


}