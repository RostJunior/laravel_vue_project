<?php
/**
 * Created by PhpStorm.
 * User: RostT
 * Date: 19.07.2018
 * Time: 14:22
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use DB;

class DBSetController extends Controller
{

    /**
     * Create content in tables db
     * (tournaments)
     * (countries)
     */
    public function createTablesDB() {
        // $this->setTournamentsAll();
        // $this->setCountriesAll();
       // $this->setUpcomingGames();
       // $this->setTeams();
       //   $this->updateTeamsLogo();
       // $this->setHistoryGames();
        $this->setTournamentsToMenu();


    }


    private function setTournamentsAll() {
        $data = DB::connection('main')->table('tournaments')
            ->leftjoin('tournaments_description', function($query) {
                $query->on('tournaments_description.content_id', 'tournaments.id')
                    ->where('tournaments_description.lang_id', 1);
            })
            ->select('tournaments.id', 'tournaments.country_id', 'tournaments.tournament_group_id', 'tournaments_description.name')
            ->get();

        DB::table('tournaments')->delete();
        $insert = [];
        foreach ($data as $d_item) {
            $insert[] = [
                'id'                  =>   $d_item->id,
                'country_id'          =>   $d_item->country_id,
                'tournament_group_id' =>   $d_item->tournament_group_id,
                'name'                =>   $d_item->name,
            ];
        }

        DB::table('tournaments')->insert($insert);

    }

    private function setCountriesAll() {
        $data = DB::connection('main')->table('countries')
            ->leftjoin('countries_description', function($query) {
                $query->on('countries_description.content_id', 'countries.id')
                    ->where('countries_description.lang_id', 1);
            })
            ->select('countries.id', 'countries.region_id', 'countries.flag', 'countries_description.name')
            ->get();

        DB::table('countries')->delete();
        $insert = [];
        foreach ($data as $d_item) {
            $insert[] = [
                'id'        =>   $d_item->id,
                'region_id' =>   $d_item->region_id,
                'flag'      =>   $d_item->flag,
                'name'      =>   $d_item->name,
            ];
        }

        DB::table('countries')->insert($insert);
    }


    private function setUpcomingGames() {

        $games = DB::connection('main')->table('games')
            ->where(function($query) {
                $query->whereDate('start', '>=', '2018-05-21')
                    ->whereDate('start', '<=', '2018-05-27');
            })
            ->select(
                'id',
                'tournament_id',
                'start',
                'team_1_id',
                'team_2_id',
                'odd_1',
                'odd_x',
                'odd_2'
            )
            ->get();

        DB::table('games_upcoming')->delete();
        $insert = [];
        foreach ($games as $d_item) {
            $insert[] = [
                'id'            =>   $d_item->id,
                'tournament_id' =>   $d_item->tournament_id,
                'start'         =>   $d_item->start,
                'team_1_id'     =>   $d_item->team_1_id,
                'team_2_id'     =>   $d_item->team_2_id,
                'odd_1'         =>   $d_item->odd_1,
                'odd_x'         =>   $d_item->odd_x,
                'odd_2'         =>   $d_item->odd_2,
            ];
        }
        DB::table('games_upcoming')->insert($insert);
    }

    private function setTeams() {
        $teams = [];
        $t1 = DB::table('games_upcoming')->distinct()->pluck('team_1_id')->toArray();
        $t2 = DB::table('games_upcoming')->distinct()->pluck('team_2_id')->toArray();

        $teams = array_unique(array_merge($t1, $t2));

        $teams_main = DB::connection('main')->table('teams_description')->whereIn('content_id', $teams)->get();
        $insert = [];
       foreach ($teams_main as $team) {
           $insert[] = [
               'id'=> $team->content_id,
               'name' => $team->name
           ];
       }

        DB::table('teams')->delete();
        DB::table('teams')->insert($insert);

    }

    private function updateTeamsLogo() {
        $team_ids = DB::table('teams')->pluck('id')->toArray();

        $logos = DB::connection('swy')->table('teams')
            ->whereIn('main_id', $team_ids)
            ->select('id', 'main_id', 'img_src')
            ->get();

        foreach ($logos as $team) {
            DB::table('teams')->where('id', $team->main_id)->update(['logo' => $team->img_src]);
        }


    }

    private function setHistoryGames() {
        ini_set('max_execution_time', 0);
        $team_ids = DB::table('teams')->pluck('id')->toArray();
        DB::table('games_old')->delete();
        $ids = [];

        foreach ($team_ids as $team_id) {
            $games = DB::connection('main')->table('games')
                ->where('team_1_id', $team_id)->orWhere('team_2_id', $team_id)
                ->select(
                    'id',
                    'tournament_id',
                    'start',
                    'team_1_id',
                    'team_2_id',
                    'odd_1',
                    'odd_x',
                    'odd_2',
                    'team_1_goals',
                    'team_2_goals'
                )->get();

            if($games->isNotEmpty()) {
                $ins = [];
                foreach ($games as $game) {
                    if(!in_array($game->id, $ids)) {
                        $ids[] = $game->id;
                        $ins[] = [
                            'id'            => $game->id,
                            'tournament_id' => $game->tournament_id,
                            'start'         => $game->start,
                            'team_1_id'     => $game->team_1_id,
                            'team_2_id'     => $game->team_2_id,
                            'odd_1'         => $game->odd_1,
                            'odd_x'         => $game->odd_x,
                            'odd_2'         => $game->odd_2,
                            'team_1_goals'  => $game->team_1_goals,
                            'team_2_goals'  => $game->team_2_goals
                        ];
                    }

                }

                DB::table('games_old')->insert($ins);

            }
        }

        dd($ins);

    }

    private function setTournamentsToMenu() {
        $tourn = DB::table('games_upcoming')->distinct()->pluck('tournament_id')->toArray();

        DB::table('tournaments')->whereIn('id', $tourn)->update(['is_menu' => 1]);
    }
}