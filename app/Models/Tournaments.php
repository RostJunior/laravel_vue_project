<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tournaments extends Model
{
    protected $table = 'tournaments';


    public static function getMenu() {
        $data = \DB::table('tournaments')
            ->leftjoin('tournament_groups', 'tournament_groups.id', 'tournaments.tournament_group_id')
            ->leftjoin('countries', 'countries.id', 'tournaments.country_id')
            ->leftjoin('country_groups', 'country_groups.id', 'countries.region_id')
            ->select(
                'tournament_groups.name as tournament_groups_name',
                'tournaments.*',
                'countries.region_id as country_region_id',
                'countries.flag as country_flag',
                'countries.name as country_name',
                'country_groups.name as country_group_name'
            )
            ->where('tournaments.is_menu', 1)
            ->orderBy('country_groups.sort')
            ->get();
        $array = [];
        if($data->isNotEmpty()) {
            $regions = $data->groupBy('country_group_name');
            $result = [];
            foreach ($regions as $reg_name => $region_items) {
                    $arr = [];
                    $countries = $region_items->groupBy('country_name');
                    foreach ($countries as $c_name => $tournaments) {
                        $arr [] = [
                            'countriesName' => $c_name,
                            'localTournaments' => $tournaments
                        ];
                    }

                $result[] = ['item' => $reg_name, 'countries' => $arr];
            }

            return $result;
        }

        return false;
    }
}
