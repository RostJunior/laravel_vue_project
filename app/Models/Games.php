<?php
/**
 * Created by PhpStorm.
 * User: RostT
 * Date: 19.07.2018
 * Time: 15:43
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use DB;

class Games extends Model
{
    protected $table = 'games';


    public static function getTopGames() {
      return  DB::table('games_upcoming')
            ->leftjoin('tournaments', 'games_upcoming.tournament_id', 'tournaments.id')
            ->leftjoin('teams as team_1', 'games_upcoming.team_1_id', 'team_1.id')
            ->leftjoin('teams as team_2', 'games_upcoming.team_2_id', 'team_2.id')
            ->select(
                'games_upcoming.*',
                'tournaments.name as tournament',
                'team_1.name as team_1_name',
                'team_1.logo as team_1_logo',
                'team_2.name as team_2_name',
                'team_2.logo as team_2_logo'
                )
            ->where('top', 1)
            ->orderBy('games_upcoming.start')
            ->get();
    }

    public static function getTournamentGames($tournament_id) {
        return  DB::table('games_upcoming')
            ->leftjoin('tournaments', 'games_upcoming.tournament_id', 'tournaments.id')
            ->leftjoin('countries', 'countries.id', 'tournaments.country_id')
            ->leftjoin('teams as team_1', 'games_upcoming.team_1_id', 'team_1.id')
            ->leftjoin('teams as team_2', 'games_upcoming.team_2_id', 'team_2.id')
            ->select(
                'games_upcoming.*',
                'tournaments.name as tournament',
                'countries.name as country',
                'team_1.name as team_1_name',
                'team_1.logo as team_1_logo',
                'team_2.name as team_2_name',
                'team_2.logo as team_2_logo'
            )
            ->where('tournament_id', $tournament_id)
            ->orderBy('games_upcoming.start')
            ->get();

    }

}