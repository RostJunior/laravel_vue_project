<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/createtables', ['uses' => 'Api\DBSetController@createTablesDB', 'as' => 'db.create_tables']);
Route::get('/menu', ['uses' => 'Api\ApiController@getMenu', 'as' => 'get.menu']);
Route::get('/games/top', ['uses' => 'Api\ApiController@getTopGames', 'as' => 'get.games.top']);
Route::get('/games/tournament/{id}', ['uses' => 'Api\ApiController@getTournamentGames', 'as' => 'get.games.tournament']);
